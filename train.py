"""
Trains a ranker for news clusters.

Usage:
    train.py [-c [-f N] [-s SCORE]] -o <MODEL> <TRAIN> ...

Options:
    -o <MODEL>      Filename of the output trained model.
    -c              Perform cross-fold validation.
    -f N            N-Cross fold validation. No model is written. [default: 10]
    -s SCORE        Report on SCORE when doing cross-fold validation. [default: f1]
    -h,--help       Shows usage instructions.
"""

import logging
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn import cross_validation
import scipy.sparse as sp
from docopt import docopt

from features import extract_features


logger = logging.getLogger()

#args = docopt(__doc__)

def fetch_clusters():
  es = elasticsearch.Elasticsearch("zookst14.science.uva.nl:8008/elasticsearch")
  fields = ["metadata." + field for field in [u'domain', u'guid', u'title', u'url', u'retrieved', u'tags', u'ncl', u'tld', u'cluster_title', u'subdomain', u'cluster_url', u'published']]
  hits = es.search(index="docdocdocs", size=10**5, fields=["url", "raw"] + fields) # TODO: start/end date
  articles = hits["hits"]["hits"]

  newsclusters = defaultdict(list)
  for a in articles:
    cluster_id = a["fields"]["metadata.ncl"][0][:15] # sometimes clusterIDs are composed of 15+ chars (but the titles are all the same, so I take all clusterIds w/ identical first 15 chars...
    clean_text = []
    paragraphs = justext.justext(a["fields"]["raw"][0], justext.get_stoplist('Dutch'))
    for paragraph in paragraphs:
        if not paragraph.is_boilerplate:
            clean_text.append(paragraph.text)
    a["fields"]["raw"][0] = " ".join(clean_text)
    newsclusters[cluster_id].append(a)

  return newsclusters


def train(newsclusters):
    """Train news cluster ranker.

    Parameters
    ----------
    newsclusters : iterable over list
        A sequence of lists of tokens.
    """
    if not isinstance(newsclusters, list):
        newsclusters = list(newsclusters)

    logger.debug("Extracting features")

    X = []
    for i, p in enumerate(newsclusters):
        # X.append(extract_features(s, vocabulary))
        X.append(extract_features(p))
    X = sp.vstack(X, format='csr')

    y = np.array(gt)
    
    clf = RandomForestClassifier(n_estimators=30)
    if args["-c"]:
        scores = cross_validation.cross_val_score(clf, X.toarray(), y, cv=int(args["-f"]), scoring=args["-s"])
        print("F1: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
    clf.fit(X.toarray(), y)
    
    return clf


if __name__ == "__main__":
    # Write pickled classifier to stdout.

    import cPickle as pickle
    import sys
    import elasticsearch
    from collections import defaultdict
    
    import justext
    
    #logging.basicConfig(level=logging.DEBUG)

    newsclusters = fetch_clusters()

    gt = []
    for newsclusterId, articleList in newsclusters.iteritems():
      print article.keys()

    #pickle.dump(train(newclusters, gt), open(args["-o"], "wb"))
