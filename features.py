import re
from numpy import sum, mean
import scipy.sparse as sp
from sklearn.preprocessing import normalize
from datetime import datetime, timedelta

def extract_features(newscluster):
    """Do feature extraction on a single news cluster.

    Parameters
    ----------
    newscluster : list of string
    """
    n_articles = len(newscluster)
    n_features = n_feature_functions
    X = sp.lil_matrix((1, n_features), dtype=float)

    for j, f in enumerate(FEATURE_FUNCTIONS):
        print f
        X[0, j] = f(newscluster)

    return X


# Sources
def has_ANP(a):
    anps = []
    for article in a:
        m = re.search(r"\bANP\b", article["fields"]["raw"][0], re.DOTALL | re.MULTILINE)
        if m:
            anps.append(1)
    return 1. * sum(anps) / len(a)
    

def n_sources(a):
    """ Number of news sources in the cluster """
    sources = []
    for article in a:
        sources.append(article["fields"]["metadata.domain"][0])
    return len(set(sources))

def source_names(a):
    """ S"""
    sources = []
    for article in a:
        sources.append(article["fields"]["metadata.domain"][0])
    # return set(sources)
    return 0.

# Articles
def n_articles(a):
    return len(a)

def ratio_articles_from_different_sources(a):
    """ Number of news sources in the cluster """
    sources = []
    for article in a:
        sources.append(article["fields"]["metadata.domain"][0])
    return 1. * len(set(sources)) / len(a)
    
def avg_length_article(a):
    lengths = []
    for article in a:
        lengths.append(len(article["fields"]["raw"][0].split()))
    return mean(lengths)

def overlap_between_articles(a):
    """ TODO: """
    return 0

def unique_words(a):
    """ Return unique words for the article """
    uw = []
    for article in a:
        uw.append(len(set(article["fields"]["raw"][0].split())))
    return mean(uw)

def cluster_lifespan_unique_days(a):
    days = []
    for article in a:
        ts = datetime.strptime(article["fields"]["metadata.published"][0], "%Y-%m-%dT%H:%M:%S+00:00")
        days.append(ts)
    days.sort()
    if len(days) > 1:
        return (days[1] - days[0]).days
    else:
        return 0

def cluster_timestamp_hours(a):
    days = []
    for article in a:
        ts = datetime.strptime(article["fields"]["metadata.published"][0], "%Y-%m-%dT%H:%M:%S+00:00")
        days.append(ts)
    days.sort()
    if len(days) > 1:
        return (days[1] - days[0]).seconds
    else:
        return 0

def title_changes(a):
    """ Tracks how many times the title of the cluster has changed """
    pass

FEATURE_FUNCTIONS = [has_ANP, n_sources, source_names, 
                     n_articles, avg_length_article, 
                     overlap_between_articles, unique_words, cluster_lifespan_unique_days,
                     cluster_timestamp_hours,
                     ratio_articles_from_different_sources,
                     # title_changes
                    ]
n_feature_functions = len(FEATURE_FUNCTIONS)


if __name__ == '__main__':
    import cPickle as pickle
    import sys
    import elasticsearch
    from collections import defaultdict
    from operator import itemgetter
    
    import justext
    
    #logging.basicConfig(level=logging.DEBUG)

    es = elasticsearch.Elasticsearch("zookst14.science.uva.nl:8008/elasticsearch")
    fields = ["metadata." + field for field in [u'domain', u'guid', u'title', u'url', u'retrieved', u'tags', u'ncl', u'tld', u'cluster_title', u'subdomain', u'cluster_url', u'published']]
    hits = es.search(index="docdocdocs", size=10**5, fields=["url", "raw"] + fields)
    articles = hits["hits"]["hits"]

    newsclusters = defaultdict(list)
    for a in articles:
      cluster_url = a["fields"]["metadata.cluster_title"][0]
      clean_text = []
      paragraphs = justext.justext(a["fields"]["raw"][0], justext.get_stoplist('Dutch'))
      for paragraph in paragraphs:
          if not paragraph.is_boilerplate:
              clean_text.append(paragraph.text)
      a["fields"]["raw"][0] = " ".join(clean_text)
      newsclusters[cluster_url].append(a)
    
    cl = newsclusters.keys()
    X = []
    for newsclusterId in cl:
        X.append(extract_features(newsclusters[newsclusterId]))
    X = sp.vstack(X, format='csr')
    X = normalize(X, axis=0, norm='l1')
    # Sum all features 
    X = X.sum(axis=1)
    
    ranking = []
    for i, newsclusterId in enumerate(cl):
        ranking.append((newsclusterId, X[i, 0]))
    for cid, score in sorted(ranking, key=itemgetter(1), reverse=True):
        print cid.encode('utf-8'), score
        for article in newsclusters[cid]:
            print "  ", article["fields"]["metadata.domain"][0]
        print 
    